<!DOCTYPE html>
<html lang="en">

<head>
    {include "./Sections/body_head.tpl"}
</head>


<body id="page-top">

{include "./Sections/nav.tpl"}


<div class="page-content">
    <div class="container">
        {block content}
        {/block}
    </div>
</div>



{include "./Sections/footer.tpl"}

</body>

{include "./Sections/body_end.tpl"}

</html>


