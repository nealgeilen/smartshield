{extends "../index.tpl"}


{block content}
    <section class="p-4">
        <h1>Wat is SmartShield.</h1>
        <p>
            In een auto gebruiken we allemaal wel eens de navigatie, deze zit meestal op een onhandige plek.
            De telefoon zit meestal in het zichtveld van de bestuurder bovenop het dashboard of tegen de voorruit. Daarnaast is een ingebouwde navigatie meestal helemaal buiten beeld in de middenconsole.

            Daarom hebben wij SmartShield bedacht, De SmartShield is een projectie van de mobiele telefoon op de voorruit. Door een extra sterke projectielamp kan de SmartShield ook overdag gebruikt worden. Daarnaast krijgt de Smart shield stroomtoevoer van de standaard 12v outlet in de auto.
        </p>
    </section>
    <section class="p-4">
        <div >
            <H1>Waarom SmartShield.</H1>
            <p>SmartShield is niet alleen veiliger, aangezien je niet weg hoeft te kijken van de weg om de navigatie te zien, maar ook makkelijker te gebruiken vanwege voice control!</p>
        </div>
    </section>
    <section>
        <h2>Heeft u vragen? Stel ze aan ons!</h2>
        <form class="form-row" form="contact" novalidate="novalidate">
            <div class="form-group col-6">
                <label>Voornaam</label>
                <input type="text" name="Voornaam" class="form-control" required="" >
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-6">
                <label>Achternaam</label>
                <input type="text" name="Achternaam" class="form-control" required="">
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-12">
                <label>Email</label>
                <input type="email" name="Email" class="form-control" required="">
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-12">
                <label>Bericht</label>
                <textarea name="Bericht" class="form-control" required=""></textarea>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-12">
                <input type="submit" class="btn btn-primary">
            </div>
        </form>
    </section>
{/block}