{extends "../index.tpl"}


{block content}
<section>
    <div class="row">
        <div class="col-md-6">
            <h1>Hoe werkt SmartShield</h1>
            <p>SmartShield werkt door een kleine projector die gemonteeerd word in de auto. Als U dan Uw telefoon koppelt, laat hij het scherm van de telefoon zien op Uw voorruit!</p>
        </div>
        <div class="col-md-6">
            <img src="/assets/images/projector.jpg">
        </div>
    </div>
</section>
{/block}