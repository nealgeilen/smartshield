{extends "../index.tpl"}


{block content}
<section>
<div class="row">
    <div class="col-md-8">
        <h1>Hoe gebruik je het?</h1>
        <p>
            Nadat er een telefoon gelinkt is, word deze laten zien op de voorruit. Maar dat is nog niet alles, deze telefoon is dan ook te besturen met gebruik van voice control!<br>
            Met voice control kan je het grootste deel van de telefoon beheren, zoals bijvoorbeeld:
        </p>
        <ul>
            <li>Muziek</li>
            <li>Navigatie</li>
            <li>Handsfree bellen</li>
            <li>Etc.</li>
        </ul>
        <p>Maar er zitten wel limitaties op. SmartShield staat niet toe om "onveilige" apps te openen, zoals bijvoorbeeld:</p>
        <ul>
            <li>Whatsapp</li>
            <li>Email</li>
            <li>Social Media</li>
            <li>Etc.</li>
        </ul>
    </div>
    <div class="col-md-4">
        <img src="/assets/images/todos.png">
    </div>
</div>
</section>
{/block}