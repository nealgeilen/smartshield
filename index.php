<?php
require_once __DIR__ . '/_app/autoload.php';


$smarty = new Smarty();

switch ($_SERVER["REQUEST_URI"]){
    case "/gebruik":
        $smarty->display(__DIR__ . DIRECTORY_SEPARATOR . "_temp" . DIRECTORY_SEPARATOR . "Pages" .DIRECTORY_SEPARATOR . "Gebruik.tpl");

        break;
    case "/techniek":
        $smarty->display(__DIR__ . DIRECTORY_SEPARATOR . "_temp" . DIRECTORY_SEPARATOR . "Pages" .DIRECTORY_SEPARATOR . "Techniek.tpl");
        break;
    case "/contact":
        $smarty->display(__DIR__ . DIRECTORY_SEPARATOR . "_temp" . DIRECTORY_SEPARATOR . "Pages" .DIRECTORY_SEPARATOR . "Contact.tpl");
        break;
    default:
    case "/":
        $smarty->display(__DIR__ . DIRECTORY_SEPARATOR . "_temp" . DIRECTORY_SEPARATOR . "Pages" .DIRECTORY_SEPARATOR . "Home.tpl");
        break;
}
